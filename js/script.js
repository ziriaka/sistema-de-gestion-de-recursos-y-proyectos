var recursos = [];
var categorias = [];
var proyectos = [];
var tareas = [];
var tipo = "";
var funcionalidades = [];
var conteiner = 0;
/*********Carga de Datos**********/

//Recursos
var r1={identificador:"1", tipo:"Empleado", nombreEC:"Alvaro", apellidoEC:"Bill", cargo:"Jefe Administracion", id: recursos.length};
recursos.push(r1);
var r2={identificador:"2", tipo:"Consultor", nombreEC:"Bernardo", apellidoEC:"Caceres", cargo:"SubJefe Administracion", id: recursos.length};
recursos.push(r2);
var r3={identificador:"3", tipo:"Empresa Externa", nombreEC:"Carlos", apellidoEC:"Diaz", cargo:"Jefe Consultas", id: recursos.length};
recursos.push(r3);

//Categorias
var c1={nombre:"Primera", limite:"50", cantidadProyectos:"50", id: categorias.length};
categorias.push(c1);
var c2={nombre:"Segunda", limite:"30", cantidadProyectos:"30", id: categorias.length};
categorias.push(c2);
var c3={nombre:"Tercera", limite:"10", cantidadProyectos:"10", id: categorias.length};
categorias.push(c3);
var c4={nombre:"Cuarta", limite:"5", cantidadProyectos:"5", id: categorias.length};
categorias.push(c4);

//Proyectos
var p1={nombreProyecto:"Primer Proyecto", fechaInicial:"08/10/15", fechaFinal:"08/15/15", categoriaSelect:"Primera", id: proyectos.length};
proyectos.push(p1);
var p2={nombreProyecto:"Segundo Proyecto", fechaInicial:"08/11/15", fechaFinal:"08/20/15", categoriaSelect:"Segunda", id: proyectos.length};
proyectos.push(p2);
var p3={nombreProyecto:"Tercer Proyecto", fechaInicial:"08/03/15", fechaFinal:"08/15/15", categoriaSelect:"Tercera", id: proyectos.length};
proyectos.push(p3);
var p4={nombreProyecto:"Cuarto Proyecto", fechaInicial:"08/20/15", fechaFinal:"08/29/15", categoriaSelect:"Cuarta", id: proyectos.length};
proyectos.push(p4);

//Tareas
var t1={proyecto:p1, nombreTarea:"Primer Tarea", horas:"67", elementosSelecc:[r1, r2], id: tareas.length};
tareas.push(t1);
var t2={proyecto:p2, nombreTarea:"Segunda Tarea", horas:"10", elementosSelecc:[r2], id: tareas.length};
tareas.push(t2);
var t3={proyecto:p1, nombreTarea:"Tercer Tarea", horas:"2", elementosSelecc:[r1], id: tareas.length};
tareas.push(t3);
var t4={proyecto:p3, nombreTarea:"Cuarta Tarea", horas:"50", elementosSelecc:[r1, r2, r3], id: tareas.length};
tareas.push(t4);
var t5={proyecto:p4, nombreTarea:"Quinta Tarea", horas:"7", elementosSelecc:[r3], id: tareas.length};
tareas.push(t5);
var t6={proyecto:p1, nombreTarea:"Sexta Tarea", horas:"5", elementosSelecc:[r1, r3], id: tareas.length};
tareas.push(t6);


/*********************************/


/*********************************/
$(document).ready(inicializo);

function inicializo(){
	general();
	$.validate();
	$.validate({
	  borderColorOnError : '#FFF',
	  addValidClassOnAll : true,
	  modules : 'date'
	});


	$(function() {
    $( ".contenedor" ).accordion({
      collapsible: true,
      heightStyle: "content",
      active: false
    });
  });
	$("#identificador").numeric();
	$("#razonSocial").numeric();
	$("#horas").numeric();
	//Datos de prueba
	//ingreso1
	//ingreso2
	//ingreso3
	listarCategoria(c1);
	listarCategoria(c2);
	listarCategoria(c3);
	listarCategoria(c4);
	//ingreso4
	listarProyecto(p1);
	listarProyecto(p2);
	listarProyecto(p3);
	listarProyecto(p4);

	listarTablaRecurso(r1);
	listarTablaRecurso(r2);
	listarTablaRecurso(r3);
	//consulta5
	//consulta6
	listarProyectoMenu6(p1);
	listarProyectoMenu6(p2);
	listarProyectoMenu6(p3);
	listarProyectoMenu6(p4);
	//consulta7
	listarRecurso(r1);
	listarRecurso(r2);
	listarRecurso(r3);
};

/*************Menu Principal (Funciones generales)***************/

function general(){
	function resetForm($form) {
	    $form.find('input:text').val('');
	    $form.find('select').val("");   
	    $(".selected").removeClass('selected');
	    $(".error").removeClass('error');
	    $(".valid").removeClass('valid');
	    $('span.form-error').hide()
	}

	$("#menu-1").on("click", function(){
		$('#subFormularioEmpresaExterna').hide();
		$('#subFormularioEmpleadoConsultor').hide();
		resetForm($("#formulario-1"));
		$("#formulario-1").fadeIn('slow');
	});

	$("#menu-2").on("click", function(){
		resetForm($("#formulario-2"));
		$("#formulario-2").fadeIn('slow');
	});

	$("#menu-3").on("click", function(){
		resetForm($("#formulario-3"));
	});

	$("#menu-4").on("click", function(){
		resetForm($("#formulario-4"));
		$("#contenedor4").fadeIn("slow");
		// Este metodo cuando se ejecuta aca, el #tablaRecursos tbody tr no devuleve nada porque no existe, aun la tabla.
		// pensar donde tendria q ir.
		var elementosSelecc = [];
		$('#tablaRecursos tbody tr').click(function(){
	        $(this).toggleClass('selected styleSelected');
	        var id = $(this).attr('id');
	        var index = elementosSelecc.indexOf(recursos[id]);
	        /*index of == -1 -> el elemento no existe dentro del array*/
	        if(index == -1){ 
	        	elementosSelecc.push(recursos[id]);
	        }
	        else{
	        	elementosSelecc.splice(index);
	        }
	    });
	    $("#ingresarTarea").on("click", function(){
			if(proyectos.length > 0){
				var proyecto = $("#proyectoSelected").val();
				var nombreTarea = $("#nombreTarea").val();
				var horas = $("#horas").val();
				if(elementosSelecc.length != 0){
					if(nombreTarea!="" && (parseInt(horas) > 0)){
						var tarea = {proyecto:proyecto, nombreTarea:nombreTarea, horas:horas, elementosSelecc:elementosSelecc};
						tareas.push(tarea);
						listarTablaTarea6(tarea);
						$('#my_dialog_message').text('Tarea ingresada con exito');
						$("#dialog-message").dialog("open");
						resetForm($("#formulario-4"));
						$("#proyectoSelected").val("");
						$("#nombreTarea").val("");
						$("#horas").val("");
					}
					else{
						$('#my_dialog_message-error').text('Ingrese los campos correctamente.');
						$("#dialog-message-error").dialog("open");
					}
				}
				else{
					$('#my_dialog_message-error').text('No selecciono recursos para ingresar la tarea, ingreselos para proseguir.');
					$("#dialog-message-error").dialog("open");
				}
				
			}
			else{
				$('#my_dialog_message-error').text('No hay proyectos ingresados para poder registrar una tarea.');
				$("#dialog-message-error").dialog("open");
			}
		});
	});

	$("#menu-5").on("click", function(){
		resetForm($("#formulario-resumen"));
		if(proyectos.length==0){
			$('#my_dialog_message-error').text('No hay proyectos ingresados para poder realizar la consulta.');
			$("#dialog-message").dialog("open");
		}
		else{
			$("#label-proyectos").css("display","none");
			$("#label-proyectos").fadeIn("slow");
			$("#proyectosOrdenados").css("display","none");
			$("#proyectosOrdenados").fadeIn("slow");
			var copiaProyectos = [];
			var listaOrdenada = [];
			contadorHoras = 0;
			for(var i = 0; i < proyectos.length; i++){
				for(var j = 0; j < tareas.length; j++){
					if(proyectos[i].nombreProyecto == tareas[j].proyecto.nombreProyecto){
						contadorHoras += parseInt(tareas[j].horas);
					}
				}
				var proyectoConHoras ={ nombreProyecto:proyectos[i].nombreProyecto,
										fechaInicial:proyectos[i].fechaInicial,
										fechaFinal:proyectos[i].fechaFinal,
										categoria:proyectos[i].categoriaSelect,
										contadorHoras:contadorHoras };

					copiaProyectos.push(proyectoConHoras);
				contadorHoras = 0;
			}
			//ordeno la lista
			for(var i=0; i<copiaProyectos.length; i++){
				listaOrdenada.push(copiaProyectos[i]);
				listaOrdenada.sort(function(a, b){
					return parseInt(b.contadorHoras) - parseInt(a.contadorHoras);
				});
			}
			if(listaOrdenada.length!=0){
				for(var i=0; i<listaOrdenada.length; i++){
					listarProyectoOrd(listaOrdenada[i]);
				}
			}
		}
	});

	$("#menu-6").on("click", function(){
		resetForm($("#formulario-6"));
		$("#label_proyecto").fadeIn();
		$("#proyectos-selected").fadeIn();
		$("#consultar6").fadeIn();
		$("#volver-seleccionar-proyecto").hide();
		$("#subFormularioConsultar6").hide();
		$("#subFormularioConsultarRecursoTarea").hide();
		$("#tareasrecursoEmpresaExterna").hide();
		$("#subFormularioConsultar6").css('display', 'none');
		$("#subFormularioConsultarRecursoTarea").css('display', 'none');
		$("#tareasrecursoEmpresaExterna").css('display', 'none');
	});

	$("#menu-7").on("click", function(){
		resetForm($("#formulario-7"));
	    $("#volver-consulta7").hide();
	    $("#subFormularioConsultar").hide();

	    $("#label_Selecc").fadeIn();
		$("#recurso7").fadeIn();
		$("#consultarRecurso").fadeIn();
	});

	/*Registro Recurso*/

	$('#identificador').on('validation', function(evt, valid) {
		var identificador = $("#identificador").val();
		var encontre = false;
		for (var i = 0; (i < recursos.length) && !encontre && valid; i++ ){
			if(recursos[i].identificador==identificador){
				encontre = true;
			}
		}
		setTimeout(function () {
			if (encontre) {
				$('#identificador').removeClass('valid');
				$('#identificador').addClass('error');
				$('#error-identificador').show();
			}else {
				$('#error-identificador').hide();
			}
		}, 100);
	});


	$("#tipo").change(function(e) {
		var existe = true;
		if(this.value == "empresa"){
			$("#subFormularioEmpleadoConsultor").css("display","none");
			$("#subFormularioEmpresaExterna").fadeIn("slow");
			$("#ingresar1").on("click", function(){
				var identificador = $("#identificador").val();
				var tipo = $("#tipo").val();
				var razonSocial = $("#razonSocial").val();
				var telefono = $("#telefono").val();
				var direccion = $("#direccion").val();
				if(identificador!=""){
					for (var i=0; i<recursos.length; i++){
						if(recursos[i].identificador==identificador){
							$('#my_dialog_message-error').text('El identificador ya ha sido ingresado, vuelva a ingresarlo.');
							$("#dialog-message-error").dialog("open");
							existe=true;
						}
						else{
							existe=false;
						}
					}
				}
				else{
					$('#my_dialog_message-error').text('Ingrese el identificador');
						$("#dialog-message-error").dialog("open");
				}
				if(!existe){
					if(identificador!="" && razonSocial !="" && telefono != "" && direccion !=""){
						var rec = {identificador: identificador, tipo: tipo, razonSocial:razonSocial, telefono:telefono, direccion:direccion, id: recursos.length};
						recursos.push(rec);
						listarTablaRecursoMostrar(rec);
						$('#my_dialog_message').text('Recurso registrado con exito.');
						$("#dialog-message").dialog("open");
						resetForm($("#formulario-1"));
					}
					else{
						$('#my_dialog_message-error').text('Existen campos incorrectos, ingreselos correctamente.');
						$("#dialog-message-error").dialog("open");
					}
				}
			});	
		}
		else{
			$("#subFormularioEmpleadoConsultor").show();
			$("#subFormularioEmpresaExterna").css("display","none");
			$("#subFormularioEmpleadoConsultor").fadeIn("slow");
			$("#ingresar2").on("click", function(){
				var identificador = $("#identificador").val();
				var tipo = $("#tipo").val();
				var nombreEC = $("#nombreEC").val();
				var apellidoEC = $("#apellidoEC").val();
				var cargo = $("#cargo").val();
				if(identificador!=""){
					for (var i=0; i<recursos.length; i++){
						if(recursos[i].identificador==identificador){
							$('#my_dialog_message-error').text('El identificador ya ha sido ingresado, vuelva a ingresarlo.');
							$("#dialog-message-error").dialog("open");
							existe=true;
						}
						else{
							existe=false;
						}
					}
				}
				else{
					$('#my_dialog_message-error').text('Ingrese el identificador');
						$("#dialog-message-error").dialog("open");
				}
				if(!existe){
					if(nombreEC!="" && apellidoEC!="" && cargo!=""){
						var rec ={identificador:identificador, tipo:tipo, nombreEC:nombreEC, apellidoEC:apellidoEC, cargo:cargo, id: recursos.length};
						recursos.push(rec);	
						listarTablaRecursoMostrar(rec);
						$('#my_dialog_message').text('Recurso registrado con exito.');
						$("#dialog-message").dialog("open");
						resetForm($("#formulario-1"));
					}
					else{
						$('#my_dialog_message-error').text('Existen campos incorrectos, ingreselos correctamente.');
						$("#dialog-message-error").dialog("open");
					}
				}
			});
		}
	});

	/*Registro Categoria - HECHO*/

	$("#ingresarCategoria").on("click", function(){
		var existe = false;
		var nombre = $("#nombre").val();
		var limite = $( "#slider" ).slider( "value" );
		var cantidadProyectos = $( "#slider" ).slider( "value" );
		if(nombre!=""){
			if(limite!="0"){
				for (var i = 0; i<categorias.length; i++) {
					if(categorias[i].nombre==nombre){
						$('#my_dialog_message-error').text('El nombre de la categoria ya existe, ingrese un nuevo nombre.');
						$("#dialog-message-error").dialog("open");
						existe=true;
					}
				}
				if(!existe){
					limite = limite;
					cantidadProyectos = cantidadProyectos;
					var cat={nombre:nombre, limite:limite, cantidadProyectos:cantidadProyectos};
					categorias.push(cat);
					listarCategoria(cat);
					$("#nombre").val("");
					$("#label").css("margin-left", (0) * 2 + "%");
					$(function() {
					   var initialValue=0, min=0, max=50;
					   $("#slider").slider({
					      value:initialValue,
					      min: min,
					      max: max,
					      step: 1,
					      slide: function( event, ui ) {
					          $("#label").text(ui.value);
					          $("#label").css("margin-left", (ui.value) * 2 + "%");
					      }
					  });
					  $("#label").text(0);
					1});
				  $("#label").text(0);
					var cantidadProyectos = $( "#slider" ).slider();
					$('#my_dialog_message').text('La categoria ha sido ingresada con exito.');
					$("#dialog-message").dialog("open");
				}
			}
			else{
				$('#my_dialog_message-error').text('Ingrese un limite adecuado');
				$("#dialog-message-error").dialog("open");
			}
			
		}
		else{
			$('#my_dialog_message-error').text('Ingrese los campos');
			$("#dialog-message-error").dialog("open");
		}
	});

	/*Registro Proyecto - HECHO*/ 

	$("#ingresarProyecto").on("click", function(){
		if(categorias.length > 0){
			var nombreProyecto = $("#nombreProyecto").val();
			var categoria = $("#categoriaSelect").val();
			var fechaInicial = $("#datepicker1").val();
			var fechaFinal = $("#datepicker2").val();
			if(nombreProyecto!="" && fechaInicial!="" && fechaFinal!="" && categoria !=null){
				if ( ($.datepicker.parseDate('mm/dd/yy', fechaFinal) -  $.datepicker.parseDate('mm/dd/yy', fechaInicial)) <= 1) {
					$('#my_dialog_message-error').text('La fecha final ingresada debe ser mayor que la fecha inicial.');
					$("#dialog-message-error").dialog("open");	
				}
				else{
					for (var i = 0; i<categorias.length; i ++) {
						if(categorias[i].nombre == categoria){ 
							if(categorias[i].cantidadProyectos > 0){
								categorias[i].cantidadProyectos--;
								var proyec = {nombreProyecto:nombreProyecto, fechaInicial:fechaInicial, fechaFinal:fechaFinal, categoriaSelect:categoriaSelect};
								proyectos.push(proyec);
								listarProyecto(proyec);
								$('#my_dialog_message').text('Se agrego el proyecto exitosamente.');
								$("#dialog-message").dialog("open");
								var nombreProyecto = $("#nombreProyecto").val("");
								var categoria = $("#categoriaSelect").val("");
								var fechaInicial = $("#datepicker1").val("");
								var fechaFinal = $("#datepicker2").val("");
							}
							else{
								$('#my_dialog_message-error').text('No se puede agregar el proyecto con dicha categoria asiganada dado que supera el limite de proyectos asignados a la categoria.');
								$("#dialog-message-error").dialog("open");
							}
						}
					};
				}	
			}
			else{
				$('#my_dialog_message-error').text('Ingrese los campos correctamente.');
				$("#dialog-message-error").dialog("open");
			}
		}
		else{
			$('#my_dialog_message-error').text('Se necesitan categorias ingresadas para poder ingresar un proyecto.');
			$("#dialog-message-error").dialog("open");
		}
	});
	$("#volver-seleccionar-proyecto").on("click", function(){
		$("#tareaslistadas tbody").html("");
		$("#recursoslistados tbody").html("");
		$("#label_proyecto").fadeIn();
		$("#proyectos-selected").fadeIn();
		$("#consultar6").fadeIn();
		$("#subFormularioConsultar6").fadeOut("slow");

	});

	$("#consultar6").on("click", function(){
		var proyectoSeleccionado = $("#proyectos-selected").val();
		if(proyectoSeleccionado!=null){
			$("#label_proyecto").hide();
			$("#proyectos-selected").hide();
			$("#consultar6").hide();
			$("#subFormularioConsultar6").fadeIn("slow");
			$("#volver-seleccionar-proyecto").css("display","none");
			$("#volver-seleccionar-proyecto").fadeIn("slow");
			for(var i=0; i<tareas.length; i++){
				if(tareas[i].proyecto.nombreProyecto == proyectoSeleccionado){
					listarTablaTarea6(tareas[i]);
				}
			}

			var tareaSelecc = "";
			$('#tareaslistadas tbody tr').click(function(){
		        // Borro la clase selected de todas las rows.
		        $('#tareaslistadas .selected').removeClass('selected');
		        $('#tareaslistadas .styleSelected').removeClass('styleSelected');
		        $("#recursoslistados tbody").html("");
		        $(this).toggleClass('selected styleSelected');
		        resetForm($("#recursoslistados"));
		        var id = $(this).attr('id');
		        tareaSelecc = tareas[id];
		        $("#subFormularioConsultarRecursoTarea").fadeIn("slow");
		        for (var i=0; i<recursos.length; i++) {
		        	arrayRecursosTarea = tareas[id].elementosSelecc;
		        	for(var j=0; j<arrayRecursosTarea.length; j++){
						if(recursos[i].identificador == tareas[id].elementosSelecc[j].identificador){
							listarTablaRecursoMostrar(recursos[i]);
						}
		        	}
		        };

		    });
		}
		else{
			$('#my_dialog_message-error').text('Debe seleccionar un proyecto para realizar la consulta.');
			$("#dialog-message-error").dialog("open");
		}
	});

	//ingreso 7

	$("#volver-consulta7").on("click", function(){
		$("#listado tbody").html("");
		$("#label_Selecc").fadeIn();
		$("#recurso7").fadeIn();
		$("#consultarRecurso").fadeIn();
		$("#subFormularioConsultar").fadeOut("slow");

	});

	$("#volver-seleccionar-proyecto").button( { icons: {primary:'ui-icon',secondary:"ui-icon-circle-arrow-w"} } );
	$("#volver-consulta7").button( { icons: {primary:'ui-icon',secondary:"ui-icon-circle-arrow-w"} } );

	$("#consultarRecurso").on("click", function(){
		var recurso = $("#recurso7").val();
		var horasTotales = 0;
		var cantidadRecursosTarea = 0;
		if(recurso!=null){
			$("#label_Selecc").hide();
			$("#recurso7").hide();

			$("#consultarRecurso").css("display","block");
			$("#consultarRecurso").fadeOut("slow");	


			$("#subFormularioConsultar").css("display","none");
			$("#subFormularioConsultar").fadeIn("slow");

			$("#listado").css("display","none");
			$("#listado").fadeIn("slow");

			$("#tareas_recurso").css("display","none");
			$("#tareas_recurso").fadeIn("slow");

			$("#suma-total").css("display","none");
			$("#suma-total").fadeIn("slow");

			$("#volver-consulta7").css("display","none");
			$("#volver-consulta7").fadeIn("slow");
			for (var i = 0; i < tareas.length; i++) {
				for (var j = 0; j < tareas[i].elementosSelecc.length; j++) {
					var partir = recurso.split(" ");
					ident_partido = partir[0];
					if(tareas[i].elementosSelecc[j].identificador==ident_partido){
						listarTablaTarea(tareas[i]);
						cantidadRecursosTarea = tareas[i].elementosSelecc.length;
						horasTotales = parseInt((tareas[i].horas)*cantidadRecursosTarea) + parseInt(horasTotales);
					};
				};
			};
			if(horasTotales==0){
				$("#subFormularioConsultar").find("h4").text("La suma de las horas totales es 0.");
				$("#subFormularioConsultar").fadeIn("slow");
				$("#listadoTareas").css("display","none");
			}
			else{
				$("#horasTotalesSumaTareas").text(horasTotales);
			}
		}
		else{
			$('#my_dialog_message-error').text('Debe seleccionar un recurso para realizar la consulta.');
			$("#dialog-message-error").dialog("open");
		}
	});
};



/*************Otras Funciones***************/

/*************Listar Entidades***************/
function listarRecurso(recurso){
	if(recurso.tipo == "empresa"){
		var ident_pasar = recurso.identificador + " " + recurso.razonSocial;
	}
	else{
		var ident_pasar = recurso.identificador + " " + recurso.nombreEC + " " + recurso.apellidoEC;
	}
	$("#recurso7").append($("<option></option>").text(ident_pasar));
};

function listarTablaRecursoMostrar(recurso){
	if(recurso.tipo == "empresa"){
		var ident_pasar = recurso.identificador + " " + recurso.razonSocial;
	}
	else{
		var ident_pasar = recurso.identificador + " " + recurso.nombreEC + " " + recurso.apellidoEC;
	}
	$("#recursoslistados").find('tbody')
	.append($('<tr id=' + recurso.id + '>')
		.append($('<td>')
				.text(ident_pasar)
		)
		.append($('<td>')
                .text(recurso.tipo)
        )
    );
}

function listarTablaRecurso(recurso){
	if(recurso.tipo == "empresa"){
		var ident_pasar = recurso.identificador + " - " + recurso.razonSocial;
	}
	else{
		var ident_pasar = recurso.identificador + " - " + recurso.nombreEC + " " + recurso.apellidoEC;
	}
	$("#listadoRecursos").find('tbody')
    .append($('<tr id=' + recurso.id + '>')
        .append($('<td>')
                .text(ident_pasar)
        )
        .append($('<td>')
                .text(recurso.tipo)
        )
    );
}


function listarTablaTarea(tarea){
	$("#listado").find('tbody')
    .append($('<tr id=' + tarea.id + '>')
         .append($('<td>')
                .text(tarea.nombreTarea)
        )
        .append($('<td>')
                .text(tarea.proyecto.nombreProyecto)
        )
        .append($('<td>')
                .text(tarea.horas)
        )	

    );
}

function listarTablaTarea6(tarea){
	$("#tareaslistadas").find('tbody')
    .append($('<tr id=' + tarea.id + '>')
         .append($('<td>')
                .text(tarea.nombreTarea)
        )
        .append($('<td>')
                .text(tarea.proyecto.nombreProyecto)
        )
        .append($('<td>')
                .text(tarea.horas)
        )	

    );
}

function listarCategoria(categoria){
	$("#categoriaSelect").append($("<option></option>").text(categoria.nombre));
};

function listarProyecto(proyecto){
	$("#proyectoSelected").append($("<option></option>").text(proyecto.nombreProyecto));
};

function listarProyectoMenu6(proyecto){
	$("#proyectos-selected").append($("<option></option>").text(proyecto.nombreProyecto));
};

function listarProyectoOrd(proyecto){//muestra lo que hay dentro del "objeto" proyecto + laas horas totales
	console.log(proyecto);
	$("#proyectosOrdenados").find('tbody')
    .append($('<tr id=' + proyecto.id + '>')
        .append($('<td>')
                .text(proyecto.nombreProyecto)
        )
        .append($('<td>')
                .text(proyecto.fechaInicial)
        )
        .append($('<td>')
                .text(proyecto.fechaFinal)
        )
        .append($('<td>')
                .text(proyecto.categoria)
        )
        .append($('<td>')
                .text(proyecto.contadorHoras)
        )
    );
};

/**********DateTimePicker*********/
$(function() {
    $( "#datepicker1" ).datepicker({
      showOn: "button",
      buttonImage: "imgs/calendar.gif",
      buttonImageOnly: true,
      buttonText: "Seleccione una fecha",
      minDate: 0,
      onSelect: function(selected) {
          $("#datepicker2").datepicker("option","minDate", selected)
        }

    });
  });


$(function() {
    $( "#datepicker2" ).datepicker({
      showOn: "button",
      buttonImage: "imgs/calendar.gif",
      buttonImageOnly: true,
      buttonText: "Seleccione una fecha",
      minDate: 0,
      onSelect: function(selected) {
      	$("#datepicker1").datepicker("option","maxDate", selected)
      }
 
    });
  });

$("#datepicker1, #datepicker2").datepicker();

$(function() {
    $( document ).tooltip();
  });

$(function() {
    $( "input[type=submit], a, button" )
      .button()
      .click(function( event ) {
        event.preventDefault();
      });
  });

/*********************************/

$("#tipo").on("click",function(){
		tipo = $("#tipo").val();
	});

/************Slider***************/
$(function() {
   var initialValue=0, min=0, max=50;
   $("#slider").slider({
      value:initialValue,
      min: min,
      max: max,
      step: 1,
      slide: function( event, ui ) {
          $("#label").text(ui.value);
          $("#label").css("margin-left", (ui.value) * 2 + "%");
      }
  });
  $("#label").text(0);
1});

    $(function() {
    $( "#dialog-message" ).dialog({
    	autoOpen:false,
        modal: true,
        buttons: {
        	Ok: function() {
        		$( this ).dialog( "close" );
        	}
        }
    });

  });

 $(function() {
    $( "#dialog-message-error" ).dialog({
    	autoOpen:false,
        modal: true,
        buttons: {
        	Ok: function() {
        		$( this ).dialog( "close" );
        	}
        }
    });
    
  });